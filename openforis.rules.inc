<?php

/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function openforis_rules_event_info() {
  return array(
    'openforis_user_add' => array(
      'label' => t('User was added to the server.'),
      'module' => 'Openforis',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who was added.')),
        'site' => array('type' => 'string', 'label' => t('The application code of the site.')),
      ),
    ),
    'openforis_user_connect' => array(
      'label' => t('User connected to a client site.'),
      'module' => 'Openforis',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who connected.')),
        'site' => array('type' => 'string', 'label' => t('The application code of the site.')),
      ),
    ),
    'openforis_user_update' => array(
      'label' => t('User was updated.'),
      'module' => 'Openforis',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who was updated.')),
        'site' => array('type' => 'string', 'label' => t('The application code of the site.')),
      ),
    ),
    'openforis_user_delete' => array(
      'label' => t('User deleted.'),
      'module' => 'Openforis',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('The user who was deleted.')),
        'site' => array('type' => 'string', 'label' => t('The application code of the site.')),
      ),
    ),
    'openforis_exclusion_update' => array(
      'label' => t('The exclusion list was updated.'),
      'module' => 'Openforis',
      'arguments' => array(
        'site' => array('type' => 'string', 'label' => t('The application code of the site.')),
      ),
    ),
  );
}