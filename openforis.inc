<?php
/**
 * Authenticate a user.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $username
 *   String. The user name or email.
 * @param $password
 *   String. The user password.
 */
function openforis_services_user_login($appcode, $name, $pass) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();

  $pass = trim($pass);
  
  // did he enter his email address
  if (valid_email_address($name)) {
    $account = user_load(array('mail' => $name, 'pass' => $pass));
  }
  
  if (!valid_email_address($name) || empty($account)) {
    $uid = db_result(db_query('SELECT uid FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE value = "%s" AND type = %d', $name, OPENFORIS_TYPE_USERNAME));
    if ($uid) {
      $account = user_load(array('uid' => $uid, 'pass' => $pass));
    }
  }
  
  // we should have a valid account by now
  if (empty($account)) {
    $response['data'] = FALSE;
    $response['code'] = 1;
    return $response;
  }

  if (drupal_is_denied('mail', $account->mail)) {
    $response['data'] = 'The name %name is registered using a reserved e-mail address and therefore could not be logged in.';
    $response['vars'] = array('%name' => $account->name);
    $response['code'] = 2;
    return $response;
  }
  
  if (drupal_is_denied('user', $name)) {
    $response['data'] = 'The name %name has been denied access.';
    $response['vars'] = array('%name' => $account->name);
    $response['code'] = 2;
    return $response;
  }
  
  if ($account->status == 0) {
    $response['data'] = 'The account is blocked';
    $response['code'] = 3;
    return $response;
  }

  $ctype = variable_get('openforis_profile_content_type', '');
  $profile = openforis_get_profile($account->uid);

  if (!empty($ctype) && !empty($profile)) {
    $content = content_types($ctype);
    $account->profile = array();
    foreach ($content['fields'] as $name => $value) {
      $account->profile[$value['field_name']] = $profile->{$value['field_name']};
    }
  }
  
  db_query('UPDATE {users} SET login = %d, access = %d WHERE uid = %d', time(), time(), $account->uid);

  $response['data'] = $account;
  $response['code'] = 0;
  return $response;  
}

/**
 * Create a user in the system.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $name
 *   String. The user name.
 * @param $mail
 *   String. The user email.
 * @param $pass
 *   String. The user password.
 * @param $values
 *   Array. The data of the new user.
 */
function openforis_services_user_create($appcode, $name, $mail, $pass, $values) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $rules = module_exists('rules') ? TRUE : FALSE;

  $response = array();
  $uid = db_result(db_query('SELECT ofv.uid FROM {openforis_field_value} ofv INNER JOIN {openforis_field} of ON ofv.fid = of.fid WHERE ofv.value = "%s" AND of.type = %d', $name, OPENFORIS_TYPE_USERNAME));
  if ($uid) {
    $response['data'] = 'There is already a user with that name.';
    $response['code'] = 1;
    return $response;
  }

  $user = user_load(array('mail' => $mail));
  if ($user) {
    $response['data'] = 'There is already a user with that email address.';
    $response['code'] = 1;
    return $response;
  }

  if (empty($pass)) {
    $pass = user_password();
  }

  $edit = array();
  $edit['status'] = 1;

  $edit['name'] = $mail;
  $edit['pass'] = $pass;
  $edit['mail'] = $mail;
  $edit['init'] = $mail;
  $edit['openforis_remote_update'] = TRUE;

  $account = user_save('', $edit);

  if (empty($account)) {
    $response['data'] = 'Could not create the user.';
    $response['code'] = 1;
    return $response;
  }
  else {
    if ($rules) {
      rules_invoke_event('openforis_user_add', $account, $appcode); 
    }
    
    watchdog('openforis', 'User %name (%email) connected to the website %site.', array('%name' => $name, '%email' => $mail, '%site' => $appcode), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

    _openforis_update_profile('profile_initial_site', $account->uid, $appcode);
    _openforis_update_profile('profile_username_'. $appcode, $account->uid, $name);
    if (isset($values['uid'])) {
      _openforis_update_profile('profile_uid_'. $appcode, $account->uid, $values['uid']);
    }
    
    if (!empty($values['epass'])) {
      db_query('UPDATE {users} SET pass = "%s" WHERE uid = %d', $values['epass'], $account->uid);
    }
    
    // TODO: return whole account? does it make sense?
    $response['data'] = $account;
    $response['code'] = 0;
    return $response;
  }
}

/**
 * Connects an existing user to a website.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service and to whom the user will be connected.
 * @param $name
 *   String. The user name.
 * @param $mail
 *   String. The user email.
 * @param $uid
 *   Int. The user id in the client website.
 */
function openforis_services_user_connect($appcode, $name, $mail, $uid) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $rules = module_exists('rules') ? TRUE : FALSE;

  $response = array();

  $user = user_load(array('mail' => $mail));
  if (empty($user)) {
    $response['data'] = 'The email address is not used by any user.';
    $response['code'] = 1;
    return $response;
  }
  
  _openforis_update_profile('profile_username_'. $appcode, $user->uid, $name);
  _openforis_update_profile('profile_uid_'. $appcode, $user->uid, $uid);

  if ($rules) {
    rules_invoke_event('openforis_user_connect', $user, $appcode); 
  }
  
  $response['code'] = 0;
  $response['data'] = '';

  return $response;
}

/**
 * Update a user in the system.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $mail
 *   String. The user email.
 * @param $values
 *   Array. The updated data.
 */
function openforis_services_user_update($appcode, $mail, $values) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $rules = module_exists('rules') ? TRUE : FALSE;

  $response = array();
  
  $user = user_load(array('mail' => $mail));
    
  if (empty($user) || $user->uid == 0) {
    $response['data'] = 'The email address is not used by any user.';
    $response['code'] = 1;
    return $response;
  }
  $oldmail = $user->mail;

  $data = array();
  $data['openforis_remote_update'] = 1;

  if (isset($values['pass']) && !empty($values['pass'])) {
    $data['pass'] = $values['pass'];
  }
  if (isset($values['mail'])) {
    $data['mail'] = $values['mail'];
    $data['name'] = $values['mail'];
  }

  if (isset($values['profile']) && count($values['profile'])) {
    $profile = openforis_get_profile($user->uid);

    if (!empty($profile)) {
      foreach ($values['profile'] as $fid => $value) {
        $profile->{$fid} = $value;
      }
      node_save($profile);
    }
  }
  
  $account = user_save($user, $data);
  if (empty($account)) {
    $response['data'] = 'Could not save the user.';
    $response['code'] = 1;
    return $response;
  }
  else {
    if (isset($values['name'])) {
      _openforis_update_profile('profile_username_'. $appcode, $account->uid, $values['name']);
    }
    if (isset($values['uid'])) {
      _openforis_update_profile('profile_uid_'. $appcode, $account->uid, $values['uid']);
    }

    $clients = services_keyauth_get_keys();
    foreach ($clients as $c) {
      if ($appcode == $c->appcode) {
        continue;
      }

      $code = 'profile_username_'. $c->appcode;
      if (isset($user->{$code})) {
        if (isset($values['openforis_username_change']) && $values['openforis_username_change'] == 'global') {
          _openforis_update_profile($code, $user->uid, $values['name']);
        }
        _openforis_queue_add($c->appcode, OPENFORIS_STATUS_UPDATED, $account->uid);
        watchdog('openforis', 'Added %user to the %site queue.', array('%user' => $user->name, '%site' => $c->appcode), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $user->uid .'/edit'));
        openforis_ping_client($c->appcode);
      }
    }

    if ($rules) {
      rules_invoke_event('openforis_user_update', $account, $appcode); 
    }

    // TODO: return whole account here?
    $response['data'] = $account;
    $response['code'] = 0;
    return $response;
  }
}

/**
 * Delete a user's connection to a website in the system.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $mail
 *   String. The user email.
 */
function openforis_services_user_delete($appcode, $mail) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $rules = module_exists('rules') ? TRUE : FALSE;

  $response = array(); 

  $uid = db_result(db_query('SELECT uid FROM {users} WHERE mail = "%s"', $mail));
  if (empty($uid)) {
    $response['data'] = 'The email address is not used by any user.';
    $response['code'] = 1;
    return $response;
  }

  $fid = db_result(db_query('SELECT ofv.fid FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE name = "profile_uid_%s" AND ofv.uid = %d', $appcode, $uid));
  if ($fid) {
    db_query('DELETE FROM {openforis_field_value} WHERE fid = %d AND uid = %d', $fid, $uid);
  }

  $fid2 = db_result(db_query('SELECT ofv.fid FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE name = "profile_username_%s" AND ofv.uid = %d', $appcode, $uid));
  if ($fid2) {
    db_query('DELETE FROM {openforis_field_value} WHERE fid = %d AND uid = %d', $fid2, $uid);
  }
  
  if (empty($fid) && empty($fid2)) {
    $response['data'] = 'The user is not connected to this website';
    $response['code'] = 2;
    return $response;
  }
  
  if ($rules) {
    rules_invoke_event('openforis_user_delete', user_load($uid), $appcode); 
  }
  
  if (!_openforis_get_accounts_num($uid)) {
    user_delete(array('openforis_remote_update' => 1), $uid);
    watchdog('openforis', 'User %email was deleted from the system.', array('%email' => $mail), WATCHDOG_NOTICE);
  }
  else {
    watchdog('openforis', 'User %email was deleted from website %site.', array('%email' => $mail, '%site' => $appcode), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $uid .'/edit'));
  }

  $response['data'] = 'The user was deleted';
  $response['code'] = 0;
  return $response;
}

/**
 * Gets the accounts of a remote user.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $mail
 *   String. The user email.
 */
function openforis_services_user_get_accounts($appcode, $mail) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();

  $user = user_load(array('mail' => $mail));
  if (empty($user)) {
    $response['data'] = 'The email address is not used by any user.';
    $response['code'] = 1;
    return $response;
  }

  $response['code'] = 0;
  $response['data'] = array();

  $clients = services_keyauth_get_keys();
  foreach ($clients as $c) {
    $code = 'profile_username_'. $c->appcode;
    $uid = 'profile_uid_'. $c->appcode;
    $link = $c->domain . '/' . $c->profilepath;
    $link = str_replace('[uid]', $user->{$uid}, $link);
    $link = str_replace('[username]', $user->{$code}, $link);
    
    if ($user->{$code}) {
      $response['data'][] = array(
        'name' => $user->{$code},
        'site' => $c->title,
        'url' => $c->domain,
        'uid' => isset($user->{$uid}) ? $user->{$uid} : 0,
        'link' => $link,
      );
    }
  }
  return $response;
}

/**
 * Searches for a user on the server. Used for validation.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $mode
 *   String. The type of search (mail or name).
 * @param $value
 *   String. The value to search for.
 */
function openforis_services_user_search($appcode, $mode, $value) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();

  if ($mode == 'name') {
    $uid = db_result(db_query('SELECT ofv.uid FROM {openforis_field_value} ofv INNER JOIN {openforis_field} of ON ofv.fid = of.fid WHERE ofv.value = "%s" AND of.type = %d', $value, OPENFORIS_TYPE_USERNAME));
  }
  else {
    $uid = db_result(db_query('SELECT u.uid FROM {users} u WHERE mail="%s" AND uid <> 1', $value));
  }
  if ($uid) {
    $user = user_load(array('uid' => $uid));
    // we fetch some more info which will be used to show the error message if we are validating
    $clients = services_keyauth_get_keys();
    foreach ($clients as $key => $data) {
      if ($data->appcode == $user->profile_initial_site) {
        $user->profile_initial_site_name = $data->title;
        $user->profile_initial_site_url = $data->domain;
      }
    }

    $response['data'] = $user;
    $response['code'] = 0;
  }
  else {
    // let's search the exclusion list
    $cnt = db_result(db_query('SELECT COUNT(eid) FROM {openforis_exclusion} WHERE type = "%s" AND value = "%s"', $mode, $value));
    if ($cnt) {
      $response['data'] = NULL;
      $response['code'] = 0;
    }
    else {
      $response['data'] = NULL;
      $response['code'] = 1;
    }
  }
  
  return $response;
}

/**
 * Update the exclusion list.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $names
 *   Array. The names to exclude.
 * @param $mails
 *   Array. The email addresses to exclude.
 */
function openforis_services_exclusion_update($appcode, $names, $mails) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $rules = module_exists('rules') ? TRUE : FALSE;

  $response = array();

  db_query('DELETE FROM {openforis_exclusion} WHERE client = "%s"', $appcode);
  foreach ($names as $name) {
    $record = new stdClass();
    $record->client = $appcode;
    $record->type = 'name';
    $record->value = $name;
    drupal_write_record('openforis_exclusion', $record);
  }
  foreach ($mails as $mail) {
    $record = new stdClass();
    $record->client = $appcode;
    $record->type = 'mail';
    $record->value = $mail;
    drupal_write_record('openforis_exclusion', $record);
  }
  
  if ($rules) {
    rules_invoke_event('openforis_exclusion_update', $appcode); 
  }

  $response['data'] = '';
  $response['code'] = 0;
  return $response;
}

/**
 * Used to ping the server.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 */
function openforis_services_ping($appcode) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();
  $response['code'] = 1;

  // are we in maintenance mode?
  if (variable_get('site_offline', 0)) {
    $response['code'] = 0;
  }
  return $response;
}

/**
 * Gets the available profiles fields on the server.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 */
function openforis_services_profile_get_fields($appcode) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();
  $fields = array();

  $ctype = variable_get('openforis_profile_content_type', '');
  if (!empty($ctype)) {
    $content = content_types($ctype);
    foreach ($content['fields'] as $name => $value) {
      $fields[$name] = array(
        'name' => $value['field_name'],
        'label' => $value['widget']['label'],
        'description' => $value['widget']['description'],
        'type' => $value['type'],
      );
    }
  }
  
  $response['data'] = $fields;
  $response['code'] = 0;

  return $response;
}

/**
 * Used to get information about the server.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 */
function openforis_services_system_information($appcode) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }
  
  $response = array();
  $response['code'] = 0;
  $response['data'] = array();
  $response['data']['openforis_network_name'] = variable_get('openforis_network_name', '');
  $response['data']['openforis_use_key'] = variable_get('services_use_key', FALSE);
  $response['data']['openforis_use_sessid'] = variable_get('services_use_sessid', FALSE);

  return $response;
}

/**
 * Gets the contents of the queue on the server.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 */
function openforis_services_queue_get($appcode) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  $response = array();

  $response['data'] = _openforis_queue_get($appcode);
  $response['code'] = 0;

  return $response;
}

/**
 * Gets the contents of the queue on the server.
 *
 * @param $appcode
 *   String. The code of the client who is calling the service.
 * @param $appcode
 *   Array. A list of queue ids to delete.
 */
function openforis_services_queue_delete($appcode, $qids) {
  if (!_openforis_validate_appcode($appcode)) {
    return array('code' => 10, 'data' => 'The application code is not valid.');
  }

  _openforis_queue_delete($qids);

  $response = array();
  $response['code'] = 0;

  return $response;
}