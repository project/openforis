<?php
// $Id$

/**
 * @author Alexandru Badiu (i@voidberg.org)
 * @file
 * OpenForis administration pages.
 *
 */

/**
 * Provides the settings form.
 *
 */
function openforis_settings() {
  $form = array();

  $form['openforis_allow_remote'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow remote logins.'),
    '#default_value' => variable_get('openforis_allow_remote', TRUE),
  );

  $form['openforis_show_user_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show OpenForis data on the user page.'),
    '#default_value' => variable_get('openforis_show_user_data', TRUE),
    '#description' => t('Shows on the user page information such as the website he came from, the usernames and the ids on other websites.'),
  );

  $form['openforis_network_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Network name'),
    '#default_value' => variable_get('openforis_network_name', ''),
    '#description' => t('The name of the network. Will be used on the client sites.'),
  );

  $form['openforis_num_retries'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of retries'),
    '#default_value' => variable_get('openforis_num_retries', 5),
    '#description' => t('The number of times the system will retry to send updated information to the clients.'),
  );
    
  $form['encryption'] = array(
    '#title'        => t('Encryption'),
    '#type'         => 'fieldset',
    '#description'  => t('OpenForis supports encrypting sensitive data. This is useful when the server is not using SSL.'),
  );

  $form['encryption']['openforis_encryption_use'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use encryption'),
    '#description' => t('Whether to use encryption or not.'),
    '#default_value' => variable_get('openforis_encryption_use', FALSE),
    '#ahah' => array(
      'path' => 'admin/user/openforis/ahah/encryption-options',
      'wrapper' => 'encryption-options',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['encryption']['options'] = array(
    '#prefix' => '<div id="encryption-options">',
    '#suffix' => '</div>',
    'settings' => array(
      '#value' => ' '
    ),
  );

  if ($form['encryption']['openforis_encryption_use']['#default_value']) {
    $form['encryption']['options']['settings'] = openforis_settings_encryption();
  }

  $form['profiles'] = array(
    '#title'        => t('User profiles'),
    '#type'         => 'fieldset',
    '#description'  => t('OpenForis supports shared profiles for the users. To use them create a content type for the profile and add fields to it.')
  );

  $nodes = node_get_types();
  $num = count($nodes);
  if ($num == 0) {
    drupal_set_message(t('You need to have at least one content type.'));
    drupal_goto('admin/user/ls_service');
  }
  else {
    $ctypes = array(0 => t('Do not use profiles.'));
    foreach ($nodes as $node) {
      $ctypes[$node->type] = $node->name;
    }

    $form['profiles']['openforis_profile_content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type to use'),
      '#options' => $ctypes,
      '#default_value' => variable_get('openforis_profile_content_type', ''),
    );  
  }

  return system_settings_form($form);
}

function openforis_settings_encryption() {
  $form['openforis_encryption_privatekey'] = array(
    '#type' => 'textfield',
    '#title' => t('Server private key'),
    '#default_value' => variable_get('openforis_encryption_privatekey', ''),
    '#description' => t('The private key that will be used to communicate with clients.'),
  );
  
  $fields = _openforis_get_fields();
  $form['openforis_encryption_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Encrypt the following fields'),
    '#options' => $fields,
    '#default_value' => variable_get('openforis_encryption_fields', array()),
  );
  
  return $form;
}

function _openforis_ahah_encryption_options() {
  $cached_form_state = array();
  $cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state);

  if (!empty($_POST['openforis_encryption_use'])) {
    $cached_form['encryption']['options']['settings'] = openforis_settings_encryption();
  }
  else {
    $output = '';
    unset($cached_form['encryption']['options']['settings']);
  }

  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);

  $form_state = array('submitted' => FALSE);
  $options = $cached_form['encryption']['options'];
  unset($options['#prefix'], $options['#suffix']);
  $options = form_builder('_openforis_ahah_encryption_options', $options, $form_state);
  $output = drupal_render($options);

  print drupal_to_js(array(
    'status' => TRUE,
    'data' => $output,
  ));
  exit;
}