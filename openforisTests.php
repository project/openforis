<?php
require_once './includes/phpunit_setup.inc';

class openforisTests extends PHPUnit_Framework_TestCase {
  public $user = NULL;
  
  public function setup() {
    //db_query("BEGIN");
  }
  
  public function teardown() {
    //db_query("ROLLBACK");
  }
  
  public function setup_modules() {
    $modules = array('services', 'services_keyauth', 'openforis', 'xmlrpc_server');
    module_enable($modules);
    module_rebuild_cache();
    drupal_install_modules($modules);
    require_once(drupal_get_path('module', 'openforis') .'/openforis.services.inc');
    
    menu_rebuild();
    node_types_rebuild();
    drupal_clear_css_cache();
  }

  public function setup_clients() {
    db_query('INSERT INTO {services_keys} VALUES ("%s", "%s", "%s", "%s", "%s")', '123456789', 'Test client 1', 'http://test1.ro', 'test1', '');
    db_query('INSERT INTO {services_keys} VALUES ("%s", "%s", "%s", "%s", "%s")', '987654321', 'Test client 2', 'http://test2.ro', 'test2', '');
    
    $record->description = t('Username on !site.', array('!site' => 'Test client 1'));
    $record->name = 'profile_username_test1';
    $record->type = OPENFORIS_TYPE_USERNAME;
    drupal_write_record('openforis_field', $record);    

    $record->description = t('Username on !site.', array('!site' => 'Test client 2'));
    $record->name = 'profile_username_test2';
    $record->type = OPENFORIS_TYPE_USERNAME;
    drupal_write_record('openforis_field', $record);    

    $record = new stdClass();
    $record->description = t('User id on !site.', array('!site' => 'Test client 1'));
    $record->name = 'profile_uid_test1';
    $record->type = OPENFORIS_TYPE_UID;
    drupal_write_record('openforis_field', $record);    

    $record = new stdClass();
    $record->description = t('User id on !site.', array('!site' => 'Test client 2'));
    $record->name = 'profile_uid_test2';
    $record->type = OPENFORIS_TYPE_UID;
    drupal_write_record('openforis_field', $record);    

    $record = new stdClass();
    $record->description = t('Initial site');
    $record->name = 'profile_initial_site';
    $record->type = OPENFORIS_TYPE_INITIAL;
    drupal_write_record('openforis_field', $record);    
  }
      
  public function test_user_create() {
    $this->setup_modules();
    $this->setup_clients();

    $result = openforis_services_user_create('test1', 'voidberg', 'i@voidberg.org', 'pass', array()); return;
    $this->assertEquals($result['code'], 0);

    $user = user_load(array('mail' => 'i@voidberg'));
    $this->assertNotEquals(FALSE, $user);

    $this->assertEquals('test1', $user->profile_initial_site, print_r($user, TRUE));
    $this->assertEquals('voidberg', $user->profile_username_test1, print_r($user, TRUE));
    
    $this->assertEquals('test1', db_result(db_query('SELECT value FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE name = "%s" AND uid = %d', 'profile_initial_site', $user->uid)));
    $this->assertEquals('voidberg', db_result(db_query('SELECT value FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE name = "%s" AND uid = %d', 'profile_username_test1', $user->uid)));

    $result = openforis_services_user_create('test1', 'voidberg', 'i@voidberg.org', 'pass', array());
    $this->assertEquals($result['code'], 1);

    $result = openforis_services_user_create('test2', 'voidberg', 'i@voidberg.org', 'pass', array());
    $this->assertEquals($result['code'], 1);
  }

  public function test_user_update() {
//    $this->setup_modules();
//    $this->setup_clients();

    $this->user = user_load(array('name' => 'i@voidberg.org', 'pass' => 'pass'));
    $this->assertNotNull($this->user);
    
    $result = openforis_services_user_update('test1', 'iii@voidberg.org', array('name' => 'testdoe'));
    $this->assertEquals($result['code'], 1, $result['data']);

    $result = openforis_services_user_update('test1', 'i@voidberg.org', array('name' => 'testdoe', 'pass' => 'pass2', 'mail' => 'ii@voidberg.org'));
    $this->assertEquals($result['code'], 0, $result['data']);

    $this->user = user_load(array('name' => 'ii@voidberg', 'pass' => 'pass2'));
    $this->assertNotNull($this->user);

    $this->assertEquals('testdoe', db_result(db_query('SELECT value FROM {openforis_field} of INNER JOIN {openforis_field_value} ofv ON of.fid = ofv.fid WHERE name = "%s" AND uid = %d', 'profile_username_test1', $this->user->uid)), print_r($this->user, TRUE));
  }

  public function test_user_auth() {
  }

  public function test_user_delete() {
  }
}